// Setup basic express server
var express = require('express')
var app = express()
var server = require('http').createServer(app)
var io = require('../..')(server)
var mysql = require('mysql')
var port = process.env.PORT || 3000
var hostSQL = process.env.HOSTSQL || 'localhost'
var portSQL = process.env.PORTSQL || '3306'
var userSQL = process.env.USERSQL || 'root'
var passSQL = process.env.PASSSQL || 'root'
var dbSQL = process.env.DBSQL || 'dondeand_dond'

server.listen(port, () => {
  console.info(`Server listening at port ${port}`)
})

var connection = mysql.createConnection({
  host     : hostSQL,
  port     : portSQL,
  user     : userSQL,
  password : passSQL,
  database : dbSQL,
})



// Routing
app.use(express.static(__dirname + '/public'))

// Chatroom
var connectedSockets = []

io.on('connection', (socket) => {
  console.info(`New user with id: ${socket.id}`)

  

  socket.on(socket.id, (id, name) => {
    socket.id = id
    socket.id_userapp = id
    socket.name = name
    socket.frineds = []

    var query = `
    SELECT if(usersapp_id = ${id}, userapp2_id, if(userapp2_id = ${id}, usersapp_id, '')) AS friend_id 
    FROM amigos 
    WHERE usersapp_id = ${id} OR userapp2_id = ${id}
    AND estatus = 3
    `

    connection.query(query, (err, rows, fields) => {

      if (err)
        return console.error(err)

      socket.friends = rows

    })

    connectedSockets.push(socket)

    /*
    console.log(io.sockets.sockets)
    
    Object.keys(io.sockets.sockets).forEach(function(id) {
        console.log("ID:",id)
    })
    */
  })

  // when the client joined.. perform this
  socket.on('joined', (latitud, longitud) => {
    console.info(`New user joined id: ${socket.id} and latitud: ${latitud}`)

    socket.latitud = latitud
    socket.longitud = longitud    

    if (typeof socket.friends === "undefined" || socket.friends.length === 0) return


    var size = socket.friends.length

    for (var i = 0; i < size; ++i) {
      var id = socket.friends[i].friend_id
      var to = `friend joined${id}`
      socketEmitTo(socket, to)
      /*
      socket.broadcast.emit(`friend joined${id}`, {
        name: socket.id,
        latitud: latitud,
        longitud: longitud
      })
      */
    }

    
    /*
    var id = socket.friends[0].friend_id
    
    //if (!io.sockets.connected[id]) return

    console.info(`emitiendo.. friend joined${id}`)

    socket.broadcast.emit(`friend joined${id}`, {
      name: socket.id,
      latitud: latitud,
      longitud: longitud
    })
    */
    /*
    socket.broadcast.emit('friend joined', {
      name: socket.id,
      latitud: latitud,
      longitud: longitud
    })
    */
    
  })

  
  socket.on('wheres my friend', (text) => {
    myfriendlocations = []

    function isFriend(element, index, array) {
      console.info(`element: ${element.friend_id}, index: ${index}`)

      for (var i = 0; i < connectedSockets.length; i++) {
        if (element.friend_id == connectedSockets[i].id) {

          myfriendlocations.push({
            id: connectedSockets[i].id,
            name: connectedSockets[i].name,
            latitud: connectedSockets[i].latitud,
            longitud: connectedSockets[i].longitud
          })

          break
        }
      }
    }

    socket.friends.find(isFriend)
    
    if (myfriendlocations.length === 0) return

    socket.emit(`my friends location${socket.id}`, {myfriends: myfriendlocations})

    console.log(myfriendlocations)

  })

  // when the client change location.. perform this
  socket.on('my location change', (latitud, longitud) => {
    console.info(`User changed location id: ${socket.id} to: ${latitud} ${longitud}`)

    socket.latitud = latitud
    socket.longitud = longitud

    if (typeof socket.friends === "undefined" || socket.friends.length === 0) return

    var size = socket.friends.length

    for (var i = 0; i < size; ++i) {
      var id = socket.friends[i].friend_id
      var to = `friend change location${id}`
      socketEmitTo(socket, to)

      /*
      socket.broadcast.emit(`friend change location${id}`, {
        name: socket.id,
        latitud: latitud,
        longitud: longitud
      })
      */
    }
    
  })

  function socketEmitTo(socket, emitTo) {
    socket.broadcast.emit(emitTo, {
        id: socket.id,
        name: socket.name,
        latitud: socket.latitud,
        longitud: socket.longitud
      })
  }
  
  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    console.info(`User disconnected with id: ${socket.id}`)

    connectedSockets.pop(socket)

    if (typeof socket.friends === "undefined" || socket.friends.length === 0) return

    var size = socket.friends.length

    for (var i = 0; i < size; ++i) {
      var id = socket.friends[i].friend_id
      socket.broadcast.emit(`friend disconneted${id}`, {
        id: socket.id
      })
    }

    /*
    var id = socket.friends[0].userapp2_d

    socket.broadcast.emit(`friend disconneted${id}`, {
      name: socket.id
    })
    */
    //socket.removeListener(socket.id)

    //console.log(socket)

  })

})

connection.connect( (err) => {
  if(!err)
      console.info("Database is connected...")
  else {
      console.error(err)
      console.error("Error connecting database...")
  }
})
